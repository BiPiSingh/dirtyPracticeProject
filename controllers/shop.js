const Product = require('../models/product.js');
const Cart = require('../models/cart.js');

exports.getLandingPage = (req, res, next) => {
    console.log('Landing page');
    Product.fetchAll( (prodArr) => {
        //console.log('Current products: ' + JSON.stringify(prodArr));
        res.render('./shop/index.ejs', {
            pageTitle: 'WebShop',
            path: '/',
            prods: prodArr
        });
    });
};

exports.getProducts = (req, res, next) => {
    Product.fetchAll( (prodArr) => {
        //console.log('Current products: ' + JSON.stringify(prodArr));
        res.render('./shop/product-list.ejs', {
            pageTitle: 'Currently offered products',
            path: '/products',
            prods: prodArr
        });
    });
};

exports.getProductDetails = (req, res, next) => {
    const prodId = req.params.productId;
    Product.findById(prodId, prod => {
        console.log(prod);
        res.render('shop/product-details.ejs', {
            pageTitle: prod.title,
            path: '/products',
            product: prod
        });
    } );
    
};

exports.getCart = (req, res, next) => {
    Cart.getCart( cart => {
        console.log(cart);
        res.render('./shop/cart.ejs', {
            pageTitle: 'Shoping Cart',
            path: '/cart',
            cart: cart
        });
    });   
}

exports.postCart = (req, res, next) => {
    console.log('Your request: ' + JSON.stringify(req.body));
    Product.findById(req.body.productId, (product) => {
        Cart.addProductToCart(req.body, product);
    });
    res.redirect('/cart');
};

exports.postDeleteFromCart = (req, res, next) => {
    console.log('Entry id received ' + req.body.entryId);
    Cart.deleteFromCartByEntryId(req.body.entryId);
    res.redirect('/cart');
};

exports.getOrders = (req, res, next) => {
    console.log('Your Orders');
    res.render('./shop/orders.ejs', {
        pageTitle: 'Orders',
        path: '/orders'
    });
}

exports.getCheckOut = (req, res, next) => {
    console.log('Chekout page');
    res.render('./shop/checkout.ejs', {
        pageTitle: 'Checkout page',
        path: '/checkout'
    });
}







