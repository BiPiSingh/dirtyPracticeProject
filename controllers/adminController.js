const Product = require('../models/product.js');

exports.getAddProduct = (req, res, next) => {
    res.render('admin/edit-product.ejs', {
        pageTitle: 'Add a new product',
        path: 'admin/add-product',
        editing: false
    });
};

exports.postAddProduct = (req, res, next) => {
    const title = req.body['product_type'];
    const desc = req.body['product_desc'];
    const price = req.body['product_price'];
    const imageUrl = req.body['image_url'];
    const id = req.body.id;
    console.log('Product being edited/added with POST request is ' + title);

    let product = new Product(title, desc, price, imageUrl, id);

    console.log('Product being processed: ' + JSON.stringify(product));
    product.save(); // saves the newly created object-instance in the array
    res.redirect('/admin/products');
};


exports.getEditProduct = (req, res, next) => {
    let editMode = req.query.editMode;
    console.log('Need to edit a product with id: ' + req.params.productId);
    //Fetching product-related data
    Product.findById(req.params.productId, (returnedProduct) => {
        if(!returnedProduct)
            return res.send('Product you are trying to edit could not be found');
        console.log('Product with id ' + returnedProduct.id + '  found in database'); 
        res.render('admin/edit-product.ejs', {
            pageTitle: 'Edit product',
            path: 'admin/add-product',
            editing: editMode,  /*editMode*/
            product: returnedProduct
        });
    });    
};

exports.getProducts = (req, res, next) => {
    Product.fetchAll( (prodArr) => {
        res.render('admin/products.ejs', {
            pageTitle: 'Administrate Products',
            path: '/admin/products',
            prods: prodArr
        });
    });
};

exports.postDeleteProduct = (req, res, next) => {
    console.log('Trying to delete a product with id ' + req.body.productId );
    Product.deleteProduct(req.body.productId);
    res.redirect('/admin/products');
}