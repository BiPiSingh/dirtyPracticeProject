const express = require('express');
const router = express.Router();

const shopController = require('../controllers/shop.js');


router.get('/', shopController.getLandingPage);
router.get('/products', shopController.getProducts);
router.get('/products/:productId', shopController.getProductDetails);
router.get('/cart', shopController.getCart);
router.post('/add-to-cart', shopController.postCart);
router.get('/orders', shopController.getOrders);
router.get('/checkout', shopController.getCheckOut);
router.post('/delete-from-cart', shopController.postDeleteFromCart);

module.exports = router;