const express = require('express');
const router = express.Router();

const adminController = require('../controllers/adminController.js');

//  /admin/add-product => get
router.get('/add-product', adminController.getAddProduct);

// /admin/edit-product => get
router.get('/edit-product/:productId', adminController.getEditProduct);

//  /admin/products => get
router.get('/products', adminController.getProducts);

//  /admin/add-product => post
router.post('/add-product', adminController.postAddProduct);

// /admin/delete-products => post
router.post('/delete-product', adminController.postDeleteProduct);

module.exports = router;