const fs = require('fs');
const path = require('path');
const rootDir = require('../utils/pathFinder.js');
const Product = require('../models/product.js');

const filePath = path.join(rootDir, 'data', 'cart.json');
const prodPath = path.join(rootDir, 'data', 'products.json');

function readCart() {
    return new Promise( (resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            return err ? reject(err) : resolve(JSON.parse(data));                
        });
    });
}

function writeCart(newCartData) {
    fs.writeFile(filePath, JSON.stringify(newCartData), err => console.log(err));    
}

module.exports = class Cart {
    static addProductToCart (reqData, product) {
        fs.readFile(filePath, 'utf8', (err, data) => {
            let cart = {
                products: [],
                totalAmmount: 0
            }
            
            if(err)
                console.log('No previous products in the cart');
                
            if(!err) {
                console.log('Fetching existing cart');
                cart = JSON.parse(data);            
            }
            console.log(`You tried to book ${reqData.prod_quantity} ${product.title}
            of size: ${reqData.prod_size}`);

            let productCost = +product.price * +reqData.prod_quantity;
            //Generates a random code
            const alphaChars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
            const alphaIndex = Math.floor( Math.random() * 25); // Selects a random character from array
            const entryId = alphaChars[alphaIndex] + Math.random();
            console.log('Entry id is ' + entryId);

            cart.products.push({
                entryId: entryId,
                ...reqData,
                title: product.title,
                desc: product.desc,
                price: product.price,
                imageUrl: product.imageUrl,
                productCost: productCost
            });
            cart.totalAmmount += productCost;
            console.log("Total to pay: " + cart.totalAmmount);

            /* fs.writeFile(filePath, JSON.stringify(cart), err => {
                if(err)
                    throw err;
            }); */
            // DRY 
            writeCart(cart);
        });
    }

    static getCart(cb) {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if(err)
                return cb({products:[], totalAmmount: 0});
                // return null;
            cb(JSON.parse(data));
        });
    }

    // Delete from cart 
/*     static deleteFromCartByEntryId(entryId) {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if(err)
                throw err;
            const existingCart = JSON.parse(data);
            const unWantedProdIndex = existingCart.products.findIndex( prod => prod.entryId === entryId);
            const dltProd = existingCart.products[unWantedProdIndex];
            let sumToBeDeducted = dltProd.price * dltProd.prod_quantity;
            existingCart.products.splice(unWantedProdIndex, 1); 
            existingCart.totalAmmount -= sumToBeDeducted;
            fs.writeFile(filePath, JSON.stringify(existingCart), err => {
                if(err)
                    throw err;
            });
        });
    } */

    // Delete from cart using promises
    static deleteFromCartByEntryId(entryId) {
        readCart().then( existingCart => {
            const unWantedProdIndex = existingCart.products.findIndex( prod => prod.entryId === entryId);
            const dltProd = existingCart.products[unWantedProdIndex];
            let sumToBeDeducted = dltProd.price * dltProd.prod_quantity;
            existingCart.products.splice(unWantedProdIndex, 1); 
            existingCart.totalAmmount -= sumToBeDeducted;
            return existingCart;
        }).then( updatedCart => {
            writeCart(updatedCart);
        }).catch('Error occured');
    }

// Using callback
    /*     static deleteFromCart(prodId, prodPrice) {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if(err) 
                return;
            let existingCart = JSON.parse(data);
            existingCart.products = existingCart.products.filter( product => product.productId !== prodId );
            existingCart.totalAmmount -= prodPrice;

            fs.writeFile(filePath, JSON.stringify(existingCart), err => {
                if(err)
                    throw err;
            });
        });
    } */

    // Using promise
    static deleteFromCartById(prodId, prodPrice) {
        readCart().then( existingCart => {
            // Find all entries with productId
            const deleteableEntries = existingCart.products.filter( entry => entry.productId === prodId );

            console.log('Products not available in the store');
            console.log(JSON.stringify(deleteableEntries));

            existingCart.products = existingCart.products.filter( product => product.productId !== prodId );
            // multiply qty with price to find right sum to be deducted

            existingCart.totalAmmount -= prodPrice; 
            return existingCart;
        })
      /*   .then( newCart => {
            fs.writeFile(filePath, JSON.stringify(newCart), err => console.log(err));
        }) */
        .then( newCart => {
            console.log('Updated cart ' + JSON.stringify(newCart));
            writeCart(newCart);
        } )
        .then( () => console.log('File deleted'))
        .catch( err =>  console.log('Error occured: ' + err));
    }
}   
 