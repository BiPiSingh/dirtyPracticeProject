const fs = require('fs');
const path = require('path');

const Cart = require('./cart.js');

const filePath = path.join(path.dirname(process.mainModule.filename), 'data', 'products.json');

const getProductsFromFile = function(cb) {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if(err) return cb([]);
        return cb(JSON.parse(data));
    });
}

module.exports = class Product {
    constructor(title, desc, price, imageUrl, id ) {
        this.title = title;
        this.desc = desc;
        this.price = price;
        this.imageUrl = imageUrl;
        this.id = id;
    }

    save() {         
        getProductsFromFile( (prodArr) => {
            if(this.id) {
                let prodExistIndex = prodArr.findIndex( p => this.id === p.id);
                prodArr[prodExistIndex] = this;
            } else {
                this.id = Math.random().toString();
                prodArr.push(this);
            }
            console.log('All products will be stored in the file');
            fs.writeFile(filePath, JSON.stringify(prodArr), (err) => {
                if(err)
                    console.log(err);               
            });
        });
    }

    static fetchAll(cb) {
        getProductsFromFile(cb);
    }
    static findById(id, cb) {
        console.log('findingById: ' + id);
        getProductsFromFile( (prodArr) => {
            let wantedProd = prodArr.find( prod => prod.id === id);
            cb(wantedProd);
        });
    }
    static deleteProduct(id) {
        console.log('received in deleteProduct ' + id);
        getProductsFromFile( prodArr => {
            let unWantedProdIndex = prodArr.findIndex( prod => prod.id === id);
            Cart.deleteFromCartById(id, prodArr[unWantedProdIndex].price);
            prodArr.splice(unWantedProdIndex, 1);
            fs.writeFile(filePath, JSON.stringify(prodArr), err => {
                if(err) console.log(err);
            });
        });
    }
};